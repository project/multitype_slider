<?php

/**
 * @file
 * Functions related to multitype slider pages.
 *
 * @ingroup multitype_slider
 * @{
 */

/**
 * Sort multitype sliders by weight, used by uasort.
 */
function multitype_slider_sort($a, $b) {
  if ($a->weight == $b->weight) {
    return 0;
  }
  return $a->weight < $b->weight ? -1 : 1;
}

/**
 * Determine if a user role should see the splash.
 */
function _multitype_slider_role_access($entity, $account = NULL) {
  if ($account === NULL) {
    $account = $GLOBALS['user'];
  }
  return multitype_slider_access('view', $entity, $account, 'multitype_slider');
}

/**
 * Determine the current page visibility.
 */
function _multitype_slider_page_visibility($entity) {
  // Convert path to lowercase. This allows comparison of the same path
  // with different case. Ex: /Page, /page, /PAGE.
  $pages = drupal_strtolower($entity->access['pages'][0]);
  $page_match = TRUE;
  $visibility = $entity->access['pages']['#visibility'];
  if ($visibility < BLOCK_VISIBILITY_PHP) {
    // Convert the Drupal path to lowercase.
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    // Compare the lowercase internal and lowercase path alias (if any).
    $page_match = drupal_match_path($path, $pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }
    // When $visibility has a value of 0 (BLOCK_VISIBILITY_NOTLISTED),
    // the block is displayed on all pages except those listed in $block->pages.
    // When set to 1 (BLOCK_VISIBILITY_LISTED), it is displayed only on those
    // pages listed in $block->pages.
    $page_match = !($visibility xor $page_match);
  }
  elseif (module_exists('php')) {
    $page_match = php_eval($block->pages);
  }
  else {
    $page_match = FALSE;
  }

  // If we have a page match, we must make sure to run it through our auto_page
  // settings to unmatch if necessary.
  if ($page_match && ($auto_paths = multitype_slider_get_auto_pages($entity))) {
    $page_match = !drupal_match_path($_GET['q'], $auto_paths);
  }
  return $page_match;
}

/**
 * Determine the current page visibility based on active locale.
 */
function _multitype_slider_locale_visibility($entity) {
  global $language;
  return empty($entity->access['locale']) || in_array($language->language, $entity->access['locale']);
}

/**
 * Return all auto pages.
 */
function multitype_slider_get_auto_pages($entity) {
  $auto_pages = &drupal_static(__FUNCTION__, array());
  $cache_key = empty($entity->oid) ? 'new' : $entity->oid;
  if (empty($auto_pages[$cache_key])) {
    $auto_pages[$cache_key] = module_invoke_all('multitype_slider_no_show_paths', $entity);
    $auto_pages[$cache_key] = implode("\n", $auto_pages[$cache_key]);
    drupal_alter('multitype_slider_no_show_paths', $auto_pages[$cache_key]);
  }
  return $auto_pages[$cache_key];
}

/**
 * Determine the visibility based on device.
 */
function _multitype_slider_device_visibility($entity) {
  if (!module_exists('mobile_detect')
      || !$entity->access['devices']['#enabled']) {
    return TRUE;
  }
  elseif (($detect = mobile_detect_get_object())
          && ($triggers = array_merge($entity->access['devices']['phones'], $entity->access['devices']['tablets']))) {
    foreach ($triggers as $trigger) {
      if ($detect->is($trigger)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Determine if the current user has already seen the splash.
 */
function _multitype_slider_has_seen($entity) {
  if (($entity->storage['cookies']['#enabled'])) {
    // @todo convert to include the machine name
    return !empty($_COOKIE['multitype_slider:' . $entity->oid]);
  }
  else {
    return FALSE;
  }
}

/**
 * Defines our app objects.
 */
class MultitypeSliderEntity extends Entity {

  /**
   * Label for multitype slider.
   *
   * @var name
   */
  public $name;

  /**
   * Weight for entity.
   *
   * @var weight
   */
  public $weight;

  /**
   * Links for entity.
   *
   * @var links
   */
  public $links;

  /**
   * Storage entity cookies.
   *
   * @var storage
   */
  public $storage;

  /**
   * Define access level.
   *
   * @var access
   */
  public $access = array();

  /**
   * Convertor.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'multitype_slider');
  }

  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * Checked entity is locked or not.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }

}

/**
 * Class MultitypeSliderEntityController.
 */
class MultitypeSliderEntityController extends EntityAPIController implements EntityAPIControllerInterface {

  /**
   * Create a new entity.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   */
  public function create(array $values = array()) {
    global $user;

    // Set the default values.
    $values += array(
      'status' => MULTITYPE_SLIDER_STATUS_ACTIVE,
      'weight' => 0,
      'access' => array(
        'roles' => _multitype_slider_default_roles(),
        'pages' => array(
          '#visibility' => BLOCK_VISIBILITY_NOTLISTED,
          0       => '',
        ),
        'devices' => array(
          '#enabled'  => FALSE,
          'phones'    => array(),
          'tablets'   => array(),
        ),
        'locale' => array(),
      ),
      'uid' => $user->uid,
    );
    $entity = parent::create($values);
    return $entity;
  }

}

/**
 * UI controller - may be put in any include file and loaded.
 */
class MultitypeSliderUIController extends EntityDefaultUIController {

  /**
   * Translates form_submit values to entity values.
   */
  public function entityFormSubmitBuildEntity($form, &$form_state) {
    $form_state[$this->entityType] = parent::entityFormSubmitBuildEntity($form, $form_state);

    // Now do our custom work to move non-fieldable values to correct places.
    $entity = $form_state[$this->entityType];
    $data = $form_state['values']['data'];

    // access: roles.
    $entity->access['roles'] = array_intersect_key(user_roles(), array_flip(array_filter($data['audience']['roles'])));

    // access: devices.
    if (array_key_exists('devices', $data)) {
      $entity->access['devices']['#enabled'] = !$data['devices']['always_trigger'];
      $entity->access['devices']['phones'] = array_values(array_filter($data['devices']['fs_phones']['phones']));
      $entity->access['devices']['tablets'] = array_values(array_filter($data['devices']['fs_tablets']['tablets']));
    }

    // access: pages.
    $data['audience']['pages']['#visibility'] = $data['audience']['pages']['visibility'];
    unset($data['audience']['pages']['visibility']);
    $entity->access['pages'] = $data['audience']['pages'];

    // access: locale.
    if (array_key_exists('locale', $data['audience'])) {
      $entity->access['locale'] = array();
      foreach ($data['audience']['locale'] as $key => $value) {
        if (!empty($value)) {
          $entity->access['locale'][$key] = $key;
        }
      }
    }

    unset($entity->data);

    return $form_state[$this->entityType];
  }

}
