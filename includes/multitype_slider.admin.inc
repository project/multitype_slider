<?php

/**
 * @file
 * Administration page callbacks for the multitype_slider module.
 *
 * @ingroup multitype_slider
 * @{
 */

/**
 * Form builder for the multitype_slider form.
 *
 * CRUD form for multitype_slider entities.
 *
 * @see multitype_slider_form_validate()
 * @see multitype_slider_form_submit()
 * @ingroup forms
 */
function multitype_slider_form($form, &$form_state, $entity, $op, $bundle, $no_js_use = FALSE) {
  $slider = NULL;
  $form['#multitype_slider'] = $entity;
  $entity_info = entity_get_info('multitype_slider');

  if (isset($form_state['build_info']['args'][0]->oid)) {
    $id = $form_state['build_info']['args'][0]->oid;
    $slider = MultitypeSlider::getById($id);
    MultitypeSlider::getById($id, $slider->name);
  }

  $form['#tree'] = TRUE;
  $form['#attributes']['enctype'] = 'multipart/form-data';

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => ($slider) ? $slider->name : '',
    '#description' => t('A unique name to identify for the menu. It must only contain lowercase letters, numbers and hyphens.'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -20,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity->type) ? $entity->type : '',
    '#maxlength' => 32,
    '#disabled' => $entity->isLocked(),
    '#machine_name' => array(
      'exists' => 'multitype_sliders_types',
      'source' => array('name'),
    ),
    '#description' => t('A unique machine-readable name for this task type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  // Add the field related form elements.
  field_attach_form('multitype_slider', $entity, $form, $form_state);

  $form['data'] = array(
    '#tree' => TRUE,
    '#weight' => 40,
  );

  // Per-path visibility.
  $form['data']['audience'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages/Audience'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
  );

  $form['data']['audience']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which roles should see this !label?', array('!label' => $entity_info['label'])),
    '#default_value' => array_keys($entity->access['roles']),
    '#options' => user_roles(),
  );

  $access = user_access('use PHP for settings');
  $options = array(
    BLOCK_VISIBILITY_NOTLISTED => t('All pages except those listed'),
    BLOCK_VISIBILITY_LISTED => t('Only the listed pages'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
    array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ));

  if (module_exists('php') && $access) {
    $options += array(BLOCK_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
    $title = t('Pages or PHP code');
    $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  else {
    $title = t('Pages');
  }
  $form['data']['audience']['pages']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show this !label on specific pages', array('!label' => $entity_info['label'])),
    '#options' => $options,
    '#default_value' => $entity->access['pages']['#visibility'],
  );
  $form['data']['audience']['pages'][0] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">' . $title . '</span>',
    '#default_value' => $entity->access['pages'][0],
    '#description' => $description,
  );

  if (module_exists('locale')) {
    $form['data']['audience']['locale'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Which locales should see this !label?', array('!label' => $entity_info['label'])),
      '#description' => t('Leave empty to show to all locales'),
      '#default_value' => $entity->access['locale'],
      '#options' => locale_language_list(),
    );
  }

  // Note this code is repeated in _multitype_slider_page_visibility.
  $auto_pages = multitype_slider_get_auto_pages($entity, TRUE);
  $form['data']['audience']['pages'][1] = array(
    '#type' => 'textarea',
    '#title' => 'Also, never show on these pages',
    '#default_value' => $auto_pages,
    '#description' => t('These have been automatically defined for you and cannot be changed. See <code>hook_multitype_slider_no_show_paths()</code>.'),
    '#disabled' => TRUE,
  );

  if (module_exists('mobile_detect')
      && ($detect = mobile_detect_get_object())) {
    $form['data']['devices'] = array(
      '#type' => 'fieldset',
      '#title' => t('Trigger Devices'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['data']['devices']['always_trigger'] = array(
      '#type' => 'checkbox',
      '#title' => t('Always trigger, regardless of device'),
      '#default_value' => !$entity->access['devices']['#enabled'],
    );
    $form['data']['devices']['fs_phones'] = array(
      '#type' => 'fieldset',
      '#title' => t('Phone Devices'),
      '#collapsible' => TRUE,
      '#collapsed' => !$entity->access['devices']['#enabled'],
    );
    $form['data']['devices']['fs_phones']['phones'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Which phones trigger this !label?', array('!label' => $entity_info['label'])),
      '#default_value' => (array) $entity->access['devices']['phones'],
      '#options' => drupal_map_assoc(array_keys($detect->getPhoneDevices())),
    );
    $form['data']['devices']['fs_tablets'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tablet Devices'),
      '#collapsible' => TRUE,
      '#collapsed' => !$entity->access['devices']['#enabled'],
    );
    $form['data']['devices']['fs_tablets']['tablets'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Which tablets trigger this !label?', array('!label' => $entity_info['label'])),
      '#default_value' => (array) $entity->access['devices']['tablets'],
      '#options' => drupal_map_assoc(array_keys($detect->getTabletDevices())),
    );
  };

  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 50,
  );

  $form['other']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#description' => t('To disable a !label and hide it from users, uncheck this box.', array('!label' => $entity_info['label'])),
    '#default_value' => $entity->status,
  );

  $form['other']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#description' => t('In the case of multiple !labels, lower numbered !labels will be presented first.', array('!labels' => $entity_info['plural label'])),
    '#default_value' => $entity->weight,
    '#delta' => 20,
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  if (!empty($entity->is_new)) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create !entity', array('!entity' => $entity_info['label'])),
    );
  }
  else {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save !entity', array('!entity' => $entity_info['label'])),
    );
  }
  return $form;
}

/**
 * Form validation handler for multitype_form_validate().
 */
function multitype_slider_form_validate($form, &$form_state) {
  // Notify field widgets to validate their data.
  // Validate the cookie lifetime.
  $multitype_slider = $form['#multitype_slider'];
  field_attach_form_validate('multitype_slider', $multitype_slider, $form, $form_state);
}

/**
 * Form submission handler for multitype_form_submit().
 *
 * @see multitype_slider_form()
 * @see multitype_slider_form_validate()
 */
function multitype_slider_form_submit($form, &$form_state) {
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity_info = entity_get_info('multitype_slider');

  // Save and go back.
  $entity->save();
  $form_state['redirect'] = $entity_info['admin ui']['path'];
}
