<?php

/**
 * @file
 * Classes file.
 *
 * @author Duynv
 */

/**
 * This is multitype slider classes.
 */
class MultitypeSlider {

  /**
   * Old entity id.
   *
   * @var Oldentityid
   */
  public $oid;

  /**
   * Name of the entity.
   *
   * @var Nameoftheentity
   */
  public $name;

  /**
   * Status of the entity.
   *
   * @var Statusoftheentity
   */
  public $status;

  /**
   * Check user access.
   *
   * @var Checkuseraccess
   */
  public $access;

  /**
   * {@inheritdoc}
   */
  public function initialize() {
    if (is_string($this->settings)) {
      $this->settings = unserialize($this->settings);
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function getAll() {
    $results = db_select('{multitype_slider}', 'mss')
      ->fields('mss')
      ->execute()
      ->fetchAll(PDO::FETCH_CLASS, 'MultitypeSlider');

    if (!count($results)) {
      return array();
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public static function getById($oid) {
    $results = db_select('{multitype_slider}', 'mss')
      ->fields('mss')
      ->condition('oid', $oid)
      ->execute()
      ->fetchAll(PDO::FETCH_CLASS, 'MultitypeSlider');

    if (!count($results)) {
      return array();
    }

    return array_shift($results);
  }

  /**
   * {@inheritdoc}
   */
  public static function getByMachineName($machine_name) {

    $results = db_select('{multitype_slider}', 'mss')
      ->fields('mss')
      ->condition('type', $machine_name)
      ->execute()
      ->fetchAll(PDO::FETCH_CLASS, 'MultitypeSlider');

    if (!count($results)) {
      return NULL;
    }

    return array_shift($results);
  }

  /**
   * {@inheritdoc}
   */
  public static function updateMachineName($oid, $name) {

    $results = db_update('{multitype_slider}', 'mss')
      ->fields(array(
        'machine_name' => $name,
      ))
      ->condition('oid', $oid)
      ->execute()
      ->fetchAll(PDO::FETCH_CLASS, 'MultitypeSlider');
  }

}
