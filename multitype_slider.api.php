<?php

/**
 * @file
 * Hooks provided by this module.
 */

/**
 * Implements hook_multitype_slider_no_show_paths().
 */
function hook_multitype_slider_no_show_paths($entity) {
  // Notice that we don't care about the entity in this invokation
  // Not on admin pages, if a module really wants a multitype page on admin
  // they can use their own hook_multitype_slider_show_alter.
  $auto_pages = "admin*\n";

  // Service endpoints break if the multitype page loads, so register them.
  if (module_exists('services')) {
    $endpoints = services_endpoint_load_all();
    foreach ($endpoints as $endpoint) {
      $auto_pages .= $endpoint->path . "*\n";
    }
  }
  return $auto_pages;
}

/**
 * Implements hook_multitype_slider_no_show_paths().
 */
function hook_multitype_slider_no_show_paths_alter(&$pages) {
  // I do want multitype to appear on admin pages!
  $pages = str_replace("admin*\n", '', $pages);
}

/**
 * Implements hook_multitype_slider_show_alter().
 */
function hook_multitype_slider_show_alter(&$oid, $path, $slider) {
  // Don't show it until after March 12, 2013
  // Note: by checking $oid first, this will be faster, in this example it's
  // somewhat trivial, but you need to be lean with this hook as it's called on
  // every page load.
  if ($oid && time() < 1363132020) {
    $oid = 0;
  }
}

/**
 * Implements hook_multitype_slider_load().
 */
function hook_multitype_slider_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a app is inserted.
 */
function hook_multitype_slider_insert(MultitypeSlider $entity) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('multitype_slider', $entity),
      'extra' => print_r($entity, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a app being inserted or updated.
 */
function hook_multitype_slider_presave(MultitypeSlider $entity) {
  $entity->name = 'foo';
}

/**
 * Responds to a app being updated.
 */
function hook_multitype_slider_update(MultitypeSlider $entity) {
  db_update('mytable')
    ->fields(array('extra' => print_r($entity, TRUE)))
    ->condition('id', entity_id('multitype_slider', $entity))
    ->execute();
}

/**
 * Responds to app deletion.
 */
function hook_multitype_slider_delete(MultitypeSlider $entity) {
  db_delete('mytable')
    ->condition('pid', entity_id('multitype_slider', $entity))
    ->execute();
}

/**
 * Alter app forms.
 */
function hook_form_multitype_slider_form_alter(&$form, &$form_state) {
  // Your alterations.
}
