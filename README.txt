The Multitype Slider entity type is a simple custom entity type that allow
management to add slider with many slider type option in the content. This
module are used to created a different type of slider.

Available Slider Option
-----------------------
  * Full Width Slider',
  * Image Slider
  * Simple Fade Slideshow
  * Image Gallery
  * Image Gallery With Vertical Thumbnail
  * Scrolling Logo Thumbnail Slider
  * Thumbnail Navigator With Arrows
  * Vertical Slider
  * Nearby Image Partial Visible Slider
  * Carousel Slider
  * Banner Slider
  * Banner Rotator

For the slider option selection you can go
<a href="https://www.jssor.com/demos/">here</a>

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
------------
Go to content link in admin menu.
1=> Content -> Multitype Slider -> Add Slider / List of Slider
2=> After Created any slider, need to go block list by below url
		admin/structure/block
3=> Now you can see your craeted slider in the "Disabled" block section. You
can assign your slider in the content or any region where you want to display
your sliders.
 
REQUIREMENTS
------------
Require to enable below modules
1) Entity (https://www.drupal.org/project/entity)
2) Image Field Caption (https://www.drupal.org/project/image_field_caption)
3) Field group multiple (https://www.drupal.org/project/field_group_multiple)
4) Field group (https://www.drupal.org/project/field_group)
5) Ctools (https://www.drupal.org/project/ctools)
