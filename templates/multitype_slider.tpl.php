<?php

/**
 * @file
 * Template file for MULTITYPE_SLIDER entities.
 *
 * @see MULTITYPE_SLIDER_preprocess_entity() for variable names
 *
 * @ingroup MULTITYPE_SLIDER
 * @{
 */
?>
<div class="slider_main">
  <div class="slider_name"><?php print $name; ?></div>
  <div class="slider_description"><?php print $slider_markup; ?></div>
  <?php if ($slider_selection_type == 'image-slider') { ?>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
  <?php } if ($slider_selection_type == 'image-gallery') { ?>
    <div id="jssor_2" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          $style = 'ms_image_gallery';
          $style_url = image_style_url($style, $value['image_uri']);
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><img data-u="thumb" src="' . $style_url . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Thumbnail Navigator -->
      <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
        <div data-u="slides">
          <div data-u="prototype" class="p" style="width:190px;height:84px;">
            <div data-u="thumbnailtemplate" class="t"></div>
            <svg viewBox="0 0 16000 16000" class="cv">
            <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
            <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
            <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
            </svg>
          </div>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
        <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
        <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
        <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
        <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_image_gallery_slider_init();</script>
  <?php } if ($slider_selection_type == 'full-width-slider') { ?>
    <div id="jssor_3" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_full_width_slider_init();</script>
  <?php } if ($slider_selection_type == 'simple-fade-slideshow') { ?>
    <div id="jssor_6" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_simple_fade_slider_init();</script>

  <?php } if ($slider_selection_type == 'image-gallery-with-vertical-thumbnail') { ?>
    <div id="jssor_7" style="position:relative;margin:0 auto;top:0px;left:0px;width:960px;height:480px;overflow:hidden;visibility:hidden;background-color:#24262e;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:720px;height:480px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          $style = 'ms_image_vertical_thumb_gallery';
          $style_url = image_style_url($style, $value['image_uri']);
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><img data-u="thumb" src="' . $style_url . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Thumbnail Navigator -->
      <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;top:0px;width:240px;height:480px;background-color:#000;" data-autocenter="2" data-scale-left="0.75">
        <div data-u="slides">
          <div data-u="prototype" class="p" style="width:99px;height:66px;">
            <div data-u="thumbnailtemplate" class="t"></div>
            <svg viewBox="0 0 16000 16000" class="cv">
            <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
            <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
            <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
            </svg>
          </div>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:270px;" data-autocenter="2">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
        <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
        <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
        <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
        <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_image_gallery_vertical_thumb_slider_init();</script>

  <?php } if ($slider_selection_type == 'scrolling-logo-thumbnail-slider') { ?>
    <div id="jssor_5" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:-3px;left:-4px;width:150px;height:62px;z-index:0;font-size:12px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
    </div>
    <script type="text/javascript">jssor_scrolling_logo_thumb_slider_init();</script>

  <?php } if ($slider_selection_type == 'thumbnail-navigator-with-arrows') { ?>
    <div id="slider1_container" style="position: relative; width: 720px;
         height: 480px; overflow: hidden;">

      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>

      <!-- Slides Container -->
      <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 720px; height: 480px;
           overflow: hidden;">
           <?php
           foreach ($slider_images as $key => $value) {
             echo '<div><img data-u="image" src="' . $value['image'] . '" /><img data-u="thumb" src="' . $value['image'] . '" /></div>';
           }
           ?>
      </div>
      <!-- thumbnail navigator container -->
      <div data-u="thumbnavigator" class="jssort07" style="width: 720px; height: 100px; left: 0px; bottom: 0px;">
        <!-- Thumbnail Item Skin Begin -->
        <div data-u="slides" style="cursor: default;">
          <div data-u="prototype" class="p">
            <div data-u="thumbnailtemplate" class="i"></div>
            <div class="o"></div>
          </div>
        </div>
        <!-- Thumbnail Item Skin End -->
        <div data-u="arrowleft" class="jssora051" style="width:40px;height:40px;top:123px;left:8px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
          </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:40px;height:40px;top:123px;right:8px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
          </svg>
        </div>
        <!--#endregion Arrow Navigator Skin End -->

      </div>
      <!--#endregion Thumbnail Navigator Skin End -->

      <!-- Trigger -->
      <script type="text/javascript">jssor_thumbnail_navigator_slider1_init();</script>
    </div>
    <!-- Jssor Slider End -->

  <?php } if ($slider_selection_type == 'vertical-slider') { ?>
    <div id="jssor_4" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:57px;left:92px;width:400px;height:250px;z-index:0;background-color:rgba(255,188,5,0.8);font-size:16px;color:#ffffff;line-height:30px;text-align:left;padding:10px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="2" data-scale="0.5" data-scale-right="0.75">
        <div data-u="prototype" class="i" style="width:12px;height:12px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
          </svg>
        </div>
      </div>
    </div>
    <script type="text/javascript">jssor_vertical_slider_init();</script>
  <?php } if ($slider_selection_type == 'nearby-image-partial-visible-slider') { ?>
    <div id="jssor_8" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_nearby_image_partial_slider_init();</script>
  <?php } if ($slider_selection_type == 'carousel-slider') { ?>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:150px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:150px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:3px;left:16px;width:150px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb057" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5000"></circle>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora073" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora073" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_carousel_slider_init();</script>
  <?php } if ($slider_selection_type == 'banner-slider') { ?>
    <div id="jssor_10" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo $module_path; ?>/svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div data-u="thumb">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Thumbnail Navigator -->
      <div data-u="thumbnavigator" style="position:absolute;bottom:0px;left:0px;width:980px;height:50px;color:#FFF;overflow:hidden;cursor:default;background-color:rgba(0,0,0,.5);">
        <div data-u="slides">
          <div data-u="prototype" style="position:absolute;top:0;left:0;width:980px;height:50px;">
            <div data-u="thumbnailtemplate" style="position:absolute;top:0;left:0;width:100%;height:100%;font-family:verdana;font-weight:normal;line-height:50px;font-size:16px;padding-left:10px;box-sizing:border-box;"></div>
          </div>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora061" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <path class="a" d="M11949,1919L5964.9,7771.7c-127.9,125.5-127.9,329.1,0,454.9L11949,14079"></path>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora061" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <path class="a" d="M5869,1919l5984.1,5852.7c127.9,125.5,127.9,329.1,0,454.9L5869,14079"></path>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_banner_slider_init();</script>
  <?php } if ($slider_selection_type == 'banner-rotator') { ?>
    <div id="jssor_11" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
      <!-- Loading Screen -->
      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
      </div>
      <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        <?php
        foreach ($slider_images as $key => $value) {
          echo '<div><img data-u="image" src="' . $value['image'] . '" /><div style="position:absolute;top:50px;left:50px;width:450px;height:62px;z-index:0;font-size:16px;color:#000000;line-height:24px;text-align:left;padding:5px;box-sizing:border-box;">' . $value['image_alt_text'] . '</div></div>';
        }
        ?>
      </div>
      <!-- Bullet Navigator -->
      <div data-u="navigator" class="jssorb053" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <path class="b" d="M11400,13800H4600c-1320,0-2400-1080-2400-2400V4600c0-1320,1080-2400,2400-2400h6800 c1320,0,2400,1080,2400,2400v6800C13800,12720,12720,13800,11400,13800z"></path>
          </svg>
        </div>
      </div>
      <!-- Arrow Navigator -->
      <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
        <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
        <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
        </svg>
      </div>
      <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
        <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
        <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
        </svg>
      </div>
    </div>
    <script type="text/javascript">jssor_banner_rotator_slider_init();</script>
  <?php } ?>
</div>
