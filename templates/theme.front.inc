<?php

/**
 * @file
 * Code for adding variable for templates.
 */

/**
 * Implements hook_preprocess_hook().
 */
function template_preprocess_multitype_slider_render(&$variables) {
  $data_properties = array();

  $options = array();
  $entity = $variables['entity'];

  drupal_add_css(drupal_get_path('module', 'multitype_slider') . '/css/multitype_slider.css');

  drupal_add_js(drupal_get_path('module', 'multitype_slider') . '/js/jssor.slider.min.js');
  drupal_add_js(drupal_get_path('module', 'multitype_slider') . '/js/multitype_slider.js');

  $slider_image = array();
  if (is_array($entity->field_slider_image) && count($entity->field_slider_image['und']) > 0) {

    foreach ($entity->field_slider_image['und'] as $key => $value) {
      $img_uri = $value['uri'];
      $slider_image[$key]['image_uri'] = $img_uri;
      $slider_image[$key]['image'] = file_create_url($img_uri);
      $slider_image[$key]['image_alt_text'] = isset($value['image_field_caption']['value']) ? $value['image_field_caption']['value'] : '';
    }
  }
  $variables['name'] = $entity->name;
  $variables['machine_name'] = $entity->type;
  $variables['slider_markup'] = $entity->multitype_slider_markup['und'][0]['value'];
  $variables['slider_selection_type'] = $entity->field_slider_type['und'][0]['value'];
  $variables['slider_images'] = $slider_image;
  $variables['module_path'] = drupal_get_path('module', 'multitype_slider');
}
